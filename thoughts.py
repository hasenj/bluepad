from peewee import *

from api import *
from app import app, db_connect
from models import BaseModel
from auth import User, require_login, attach_user
from content import Content
from comments import Thread, Comment
import comments

class Thought(BaseModel):
    """ What other sites call "status update"

    A sort of shortform (optinally long; not size limit; just like Google+)
    writing, usually just sharing a thought or an idea, but could be a
    full-blown article.

    Uses the "content" as a blob to store the text content + its history

    Has its own timestamp which is just the timestamp of the first share. We
    can detect edits by seeing the content's timestamp is newer than the
    thought's timestamp
    """
    author = ForeignKeyField(User, related_name='thoughts')
    original_author = ForeignKeyField(User, related_name='_thoughts_and_shares') # for sharing thoughts of others
    timestamp = DateTimeField() # date of first publishing; will be used for detecting "edits" by comparing it with the content timestamp
    content = ForeignKeyField(Content)
    thread = ForeignKeyField(Thread, null=True)

    @classmethod
    def new(cls, author, text):
        """Create a new thought object"""
        content = Content.new(author, text)
        thread = Thread.create(op=author)
        return cls.create(author=author, original_author=author, content=content, timestamp=now(), thread=thread)

    def edit(self, new_text):
        """Make changes to the text content of the thought.

        Does not change the timestamp. Only creates a new content blob by
        forking the existing one and then refers to it.

        This is not a "view" so it assumes the intention is for this to be
        called when the actual author (authenticated (no pun intended)) makes
        the change
        """
        self.content = self.content.fork(self.author, new_text)
        self.save()

    def has_edits(self):
        """Returns a boolean indicating where this thought has been edited after it was posted.

        edit detection uses the content timestamp"""
        return self.timestamp < self.content.timestamp

    def tojson(self):
        thread = self.get_or_create_thread()
        comments_json = meta_objects_dict(thread.comments)
        return dict(id=self.id, author=self.author.tojson(), author_id=self.author.id,
                original_author=self.original_author.tojson(), original_author_id=self.original_author.id,
                text=self.content.text, content=self.content.tojson(),
                timestamp=self.timestamp.isoformat(), has_edits=self.has_edits(),
                thread_id=thread.id, comments=comments_json)

    def get_or_create_thread(self):
        if not self.thread:
            self.thread = Thread.create(op=self.author)
            self.save()
        return self.thread

    class Meta:
        order_by = ('-timestamp',)

# safe to call multiple times
def init():
    db_connect()
    if not Thought.table_exists():
        print "Creating thought table"
        Thought.create_table()

init()

# ----- views -------

import sockets

def create_thought(text):
    thought_dict = Thought.new(request.user, text).tojson()
    sockets.broadcast_update('thoughts:{user_id}'.format(user_id=request.user.id), thought_dict)
    return jsonify(thought_dict)

# XXX: NOT USED!
def edit_thought(thought_id, new_text):
    thought = Thought.get(Thought.id == thought_id)
    if thought.author.id != request.user.id:
        return forbidden("You can't edit this thought")
    # food for thought (no pun intended): we could enforce history integrity by
    # requiring the user to supply the id of the content blob he's changing as
    # a version indicator
    thought.edit(new_text)
    return jsonify(thought.tojson())

def query_thoughts(author_id, before=None):
    query = Thought.select().where(Thought.author == author_id)
    if before:
        query = query.where(Thought.id < before)
    return query

def list_thoughts(author_id, before=None):
    return jsonify_list(query_thoughts(author_id, before))

def add_comment(thought_id, text):
    thought = Thought.get(Thought.id == thought_id)
    thread = thought.get_or_create_thread()
    return comments.create_comment(thread.id, text) # minor issue: duplicate the thread query?

def list_comments(thought_id, before=None):
    thought = Thought.get(Thought.id == thought_id)
    thread = thought.get_or_create_thread()
    return comments.list_comments(thread.id, before=before)

@app.route("/thought/", methods=["GET", "POST"])
@require_login
def thought_api():
    if request.method == "GET":
        author_id = request.args.get('author_id', request.user.id)
        before = request.args.get('before', None)
        return list_thoughts(author_id, before=before)
    elif request.method == "POST":
        return create_thought(request.json['text'])

@app.route("/thought/<int:thought_id>", methods=["PATCH", "DELETE"])
@require_login
def single_thought_api(thought_id):
    thought = Thought.get(Thought.id == thought_id)
    if thought.author.id != request.user.id:
        abort(forbidden("You can't edit this thought"))
    if request.method == "DELETE":
        thought.delete_instance()
        return jsonify({})
    elif request.method == "PATCH":
        thought.edit(request.json['text'])
        return jsonify({})


@app.route("/thought/<int:thought_id>/comment", methods=["GET", "POST"])
@require_login
def thought_comments_api(thought_id):
    if request.method == "GET":
        before = request.args.get('before', None)
        return list_comments(thought_id, before=before)
    if request.method == "POST":
        return add_comment(thought_id, request.json['text'])

# ---------------------------------------------------
# -----------  Searching  ---------------------------
# ---------------------------------------------------

def search_thoughts(term=''):
    query = Thought.select().join(Content)
    if term:
        qterm = '%' + term + '%'
        query = query.where(Content.text ** qterm)
    return query

@app.route("/search", methods=["GET"])
@attach_user
def search():
    term = request.args.get('term', '')
    query = search_thoughts(term=term)
    return jsonify_list(query)
