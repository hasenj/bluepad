from peewee import *
from flask import abort

from api import *
from app import app, db_connect
from models import BaseModel
from auth import User, require_login
from content import Content

class Thread(BaseModel):
    """A discussion thread"""
    op = ForeignKeyField(User, related_name='threads') # OP (person who started the thread)

class Comment(BaseModel):
    """A post participating in a discussion"""
    thread = ForeignKeyField(Thread, related_name='comments')
    author = ForeignKeyField(User, related_name='comments')
    timestamp = DateTimeField() # date of first publishing; will be used for detecting "edits" by comparing it with the content timestamp
    content = ForeignKeyField(Content)

    @classmethod
    def new(cls, thread, author, text):
        content = Content.new(author, text)
        return cls.create(author=author, thread=thread, content=content, timestamp=now())

    # XXX duplicate of Thought.edit
    def edit(self, new_text):
        """Make changes to the text content of the thought.

        Does not change the timestamp. Only creates a new content blob by
        forking the existing one and then refers to it.

        This is not a "view" so it assumes the intention is for this to be
        called when the actual author (authenticated (no pun intended)) makes
        the change
        """
        self.content = self.content.fork(self.author, new_text)
        self.save()

    # XXX duplicate of Thought.has_edits
    def has_edits(self):
        """Returns a boolean indicating where this thought has been edited after it was posted.

        edit detection uses the content timestamp"""
        return self.timestamp < self.content.timestamp

    # XXX consider changing the `tojson` convention to `todict`
    def tojson(self):
        return dict(id=self.id, author=self.author.tojson(), author_id=self.author.id,
                thread_id=self.thread.id,
                text = self.content.text, content=self.content.tojson(),
                timestamp=self.timestamp.isoformat(), has_edits=self.has_edits())

    def __repr__(self):
        # return "<thread_id: {thread_id}, content: {content}>".format(thread_id=self.thread.id, content=self.content)
        return "<comment:{id}>".format(id=self.id)

    class Meta:
        order_by = ('-id',)

# safe to call multiple times
def init():
    db_connect()
    if not Thread.table_exists():
        print "Creating thread table"
        Thread.create_table()
    if not Comment.table_exists():
        print "Creating comment table"
        Comment.create_table()

init()

# ----- views -------

import sockets

def create_comment(thread_id, text):
    text = text.strip()
    if not text:
        abort(bad_request("Empty comments not allowed"))
    thread = Thread.get(Thread.id == thread_id)
    comment_dict = Comment.new(thread, request.user, text).tojson()
    sockets.broadcast_update('comments:{thread_id}'.format(thread_id=thread_id), comment_dict)
    return jsonify(comment_dict)

def edit_comment(comment_id, new_text):
    comment = Comment.get(Comment.id == comment_id)
    if comment.author.id != request.user.id:
        return forbidden("You can't edit this comment")
    comment.edit(new_text)
    return jsonify(comment.tojson())

def list_comments(thread_id, before=None):
    thread = Thread.get(Thread.id == thread_id)
    query = thread.comments
    if before:
        query = query.where(Comment.id < before)
    return jsonify_list(query)

@app.route("/thread/<int:thread_id>/comment", methods=["GET", "POST"])
@require_login
def comments_api(thread_id):
    if request.method == "GET":
        before = request.args.get('before', None)
        return list_comments(thread_id, before=before)
    if request.method == "POST":
        return create_comment(thread_id, request.json['text'])

