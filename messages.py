# models for messages & chat

from peewee import *
from flask import session, request, abort, jsonify, Response

from app import app, db_connect
from models import BaseModel, create_table
from auth import User, require_login, account_change_signal
from api import *
import sockets


class Conversation(BaseModel):
    key = CharField(primary_key=True)

    @classmethod
    def key_for(cls, *users):
        sorted_ids = sorted((id_of_object(u) for u in users))
        key = "|".join((str(id) for id in sorted_ids))
        return key

    @classmethod
    def new(cls, *users):
        return cls.create(key=cls.key_for(*users))

    @classmethod
    def between(cls, *users):
        """Get the conversation between given users.
        Or create one if none exists"""
        try:
            return cls.get(cls.key == cls.key_for(*users))
        except cls.DoesNotExist:
            return cls.new(*users)

    def participant_ids(self):
         return [int(id) for id in self.key.split("|")]

    def participants(self):
        ids = self.participant_ids()
        query = User.select().where(User.id << ids)
        return query

    def has_user(self, user):
        return self.has_user_id(user.id)

    def has_user_id(self, user_id):
        return user_id in self.participant_ids();

    def basicjson(self):
        participants = self.participants()
        # inject online status
        for p in participants:
            p.add_json_fields(is_online=is_user_online(p.id))
        messages = self.messages.order_by(Message.id.desc())
        # Note: messages are returned as a meta_objects dict, while
        # participants are returned as a plain list
        return dict(key=self.key, participants=json_list(participants),
                messages=meta_objects_dict(messages))

    def add_user_fields(self, user_id):
        is_muted = follow.is_conversation_muted(user_id, self)
        self.add_json_fields(is_muted=is_muted)


def get_conversation_for_user(user_id, conversation_key):
    conversation = Conversation.get(Conversation.key == conversation_key)
    if not conversation.has_user_id(user_id):
        raise Conversation.DoesNotExist()
    conversation.add_user_fields(user_id)
    return conversation

class Message(BaseModel):
    conversation = ForeignKeyField(Conversation, related_name='messages')
    sender = ForeignKeyField(User)
    timestamp = DateTimeField()
    text = TextField() # don't use the content model; messages are not editable once sent!

    def recipients(self):
        return self.conversation.participants().where(User.id != self.sender.id)

    def tojson(self):
        recipients = list(self.recipients())
        # Just like conversation participants: the message recipient list is a
        # plain list, not meta + objects
        return dict(conversation_key=self.conversation.key,
                recipients=json_list(recipients),
                timestamp=self.timestamp.isoformat(),
                sender_id=self.sender.id, sender=self.sender.tojson(),
                text=self.text)

    def __repr__(self):
        return "<{sender}@{convo}: {msg}>".format(sender=self.sender.disp_name(), msg=quote(truncate(self.text, 25)), convo=self.conversation.key)

def is_user_online(user):
    """Is this user currently online?
    #XXX filter it so that blocked users can't see online status
    """
    user_id = id_of_object(user)
    return sockets.ChatNamespace.is_user_online(user_id)

def send_message(user, conversation, text):
    """Add a message to the conversation from 'user'"""
    # check the user is in the conversation
    if not conversation.has_user(user):
        raise forbidden("You are not in this conversation")
    text = text.strip()
    if not text:
        raise bad_request("Can't send empty message")
    message = Message.create(sender=user, conversation=conversation, timestamp=now(), text=text)
    sockets.send_chat_message(message) # this is deferred
    return message

def init():
    db_connect()
    create_table(Conversation)
    create_table(Message)

init()

# ------ views ------

# Conversation operations:
#   GET: normal list op
#   POST: write a new message on this chat/convo
def conversation_ops(conversation):
    """To be used inside request handlers"""
    conversation.add_user_fields(request.user)
    if request.method == "GET":
        return jsonify(conversation.tojson())
    if request.method == "POST":
        text = request.json.get('text', '')
        try:
            message = send_message(request.user, conversation, text)
            return jsonify(message.tojson())
        except Response as r:
            abort(r)

# XXX weird cyclical import .. used above in Conversation class, but import
# must come here
import follow

# First, a simple api for talking to a single user
# for use in chatting
@app.route("/conversation_with/<int:other_user_id>", methods=["GET", "POST"])
@require_login
def conversation_with_user(other_user_id):
    conversation = Conversation.between(request.user, other_user_id)
    return conversation_ops(conversation)

@app.route("/conversation/<key>", methods=["GET", "POST"])
@require_login
def conversation_api(key):
    conversation = get_conversation_for_user(request.user.id, key)
    return conversation_ops(conversation)

# TODO do this in the server database instead of the session
@app.route("/add_conversation/<key>", methods=["POST"])
@require_login
def add_conversation_to_session(key):
    if 'conversations' in session and session['conversations'] is None: # bugfix
        session.pop('conversations', None)
    # XXX why are we querying here exactly? we don't seem to be using the result
    # conversation = get_conversation_for_user(request.user.id, key)
    session['conversations'] = session.get('conversations', set()).union(set([key]))
    return jsonify({})

# TODO do this in the server database instead of the session
@app.route("/remove_conversation/<key>", methods=["POST"])
@require_login
def remove_conversation_from_session(key):
    # XXX why are we querying here exactly? we don't seem to be using the result
    # conversation = get_conversation_for_user(request.user.id, key)
    session['conversations'] = session.get('conversations', set()).difference(set([key]))
    return jsonify({})

@app.route("/is_online/<int:user_id>", methods=["GET"])
@require_login
def get_online_status(user_id):
    is_online = is_user_online(user_id)
    return jsonify(dict(is_online=is_online))

def get_chats():
    if not getattr(request, 'user', None): return []
    conversations = []
    for key in session.get('conversations', set()):
        try:
            conversations.append(get_conversation_for_user(request.user.id, key))
        except Conversation.DoesNotExist:
            # XXX remove the key? or what?
            pass
    return json_list(conversations)

# do we actually need this?
@account_change_signal.connect_via(app)
def clear_session_conversations(sender):
    session.pop('conversations', None)
