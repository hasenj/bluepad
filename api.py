""" This module is not for prividing a restful api; it's an api for us to user internally to have consistent data formats

    For example, this modules helps us in creating a consistent json response formats.

"""
from __future__ import division
from math import ceil

from flask import abort, jsonify

def error_response(status_code, error_message, error_slug=""):
    """Shortcut to return json error responses"""
    resp = jsonify(dict(error=error_message, slug=error_slug))
    resp.status_code = status_code
    return resp

def bad_request(*args, **kwargs):
    return error_response(400, *args, **kwargs)

def unauthorized(*args, **kwargs):
    return error_response(401, *args, **kwargs)

def forbidden(*args, **kwargs):
    return error_response(403, *args, **kwargs)

import datetime

def now(): # utcnow
    return datetime.datetime.utcnow()

import gevent
from functools import wraps

def deferred(fn):
    """decorator that turns functions into deferred functions so they run as if
    they were called via `go func(....)`"""
    @wraps(fn)
    def go(*args, **kwargs):
        gevent.spawn(fn, *args, **kwargs)
    return go

# helpers for json list responses

from flask import request, jsonify

# helper to get the 'page' arg from the GET params
def get_page_arg():
    page = request.args.get('page', 1)
    try: return int(page)
    except: return 1 # invalid value! just default to 1

def json_list(list):
    if list:
        return [item.tojson() for item in list]
    else:
        return []

def meta_dict(page, per_page, count):
    return {
        "page": page,
        "pages": ceil(count / per_page),
        "count": count
    }

def meta_objects_dict(query, page=1, per_page=10):
    """Returns a dictionary with "meta" and "objects" fields"""
    objects = query.paginate(page, per_page)
    count = query.count()
    return {
        "meta": meta_dict(page=page, per_page=per_page, count=count),
        "objects": json_list(objects)
    }

def jsonify_list(query):
    """Return a standard json list response out of a database query"""
    return jsonify(meta_objects_dict(query, page=get_page_arg()))

# some string methods ..
def quote(str):
    return '"' + str.replace('"', r'\"') + '"'

def truncate(string, limit, trailing='...'):
    threshold = limit + len(trailing)
    if len(string) > threshold:
        return string[:limit] + trailing
    else:
        return string

def readable_timestamp(ts):
    return ts.strftime("%Y.%m.%d|%I:%M%p").lower()

def id_of_object(obj):
    """Get the id of the object, or, noramlize object/string/int all to become integers

        This helper allows functions and methods to take an object then get its
        id, in such a way that if the caller only has the id, it can send the
        id directly without first grabbing the object from the database just so
        that the receiver method ends up getting only its id

        XXX: Not sure if this is actually useful or just pointless premature optimization.
        We'll see
    """
    if isinstance(obj, (int, long)):
        return obj
    if isinstance(obj, basestring):
        try:
            return int(obj)
        except:
            # can't parse it to int?! maybe it's a string key!
            # XXX not robust enough ..
            return obj
    return obj.id

def query_first(query):
    if query.count() == 0:
        return None
    return list(query)[0]

# XXX not properly thought out
import random
def sample_query(query, sample_size=10):
    query = query.limit(100)
    return random.sample(list(query), sample_size)
