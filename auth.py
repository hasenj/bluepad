import os, re
from functools import wraps

from peewee import *

from api import *
from app import bcrypt, db_connect
from models import BaseModel

# Note: there are other imports down below around line 45

class User(BaseModel):
    username = CharField(index=True)
    password = CharField()
    email = CharField(index=True, default='')
    email_confirmed = BooleanField(default=False)
    name = CharField(default='') # display name
    is_admin = BooleanField(default=False) # for most people this will be false!

    def set_password(self, pw):
        """hashes password before setting it"""
        self.password = bcrypt.generate_password_hash(pw)
    def check_password(self, pw):
        """Check the given password matches the stored hashed password"""
        return bcrypt.check_password_hash(self.password, pw)

    def disp_name(self):
        return self.name or self.username

    def __repr__(self):
        return "{name} <{email}>".format(name=self.disp_name(), email=self.email)

    def basicjson(self):
        return dict(username=self.username, name=self.name, email=self.email, id=self.id)

    @classmethod
    def by_username(cls, username):
        try:
            return cls.get(cls.username==username)
        except cls.DoesNotExist:
            return None

def random_password():
    # bcrypt generates a different hash everytime ..
    # we're using the hash generation to get a random alphanumerical string
    # though os.urandom thing is also suitable to use as a password all on its own
    return bcrypt.generate_password_hash(os.urandom(10))[14:24]

# safe to call multiple times
def init():
    db_connect()
    if not User.table_exists():
        print "Creating user table"
        User.create_table()
    # create a demo user
    if User.select().where(User.username == "demo").count() == 0:
        print "Creating demo user"
        demo = User(username="demo", email="demo@example.com")
        demo.set_password(random_password())
        demo.save()

init()

# --- session stuff -----

from flask import request, session, abort, jsonify
from app import app

# signals for auth
import blinker

auth_sns = blinker.Namespace()
account_change_signal = auth_sns.signal('account-changed')

# get the logged in user
def get_user_id():
    if 'user_id' in session:
        return session['user_id']
    else:
        return None

def set_user_id(user_id):
    if user_id:
        session['user_id'] = user_id # login!
        add_user_id(user_id) # add it to multiple logins ..
        renew_csrf_token()
    else:
        session.pop('user_id', None) # logout!
        session.pop('account_list', None) # logout from other accounts as well
        clear_csrf_token()
    account_change_signal.send(app)

def add_user_id(user_id):
    """Add this id to the list of logged in accounts available for fast switching"""
    account_ids = session.get('account_list', set())
    account_ids.add(user_id)
    session['account_list'] = account_ids

def can_switch_to(user_id):
    """Returns true if user_id exists in account list"""
    account_ids = session.get('account_list', set())
    return bool(user_id in account_ids)

# CSRF protection
from uuid import uuid4
def csrf_token_gen():
    return str(uuid4())
def renew_csrf_token():
    session['csrf_token'] = csrf_token_gen()
def clear_csrf_token():
    session.pop('csrf_token', None)
@app.before_request
def csrf_session():
    # logged in but no token? make one
    if get_user_id() and not session.get('csrf_token', None):
        renew_csrf_token()
    # logged out but has a token? no good! clear it
    if not get_user_id() and session.get('csrf_token', None):
        clear_csrf_token()

@app.before_request
def csrf_check():
    # if a user is logged in and the request makes changes, enforce csrf protection
    if get_user_id() and request.method in ['POST', 'PUT', 'DELETE']:
        # the client must put the csrf token in a header
        request_token = request.headers.get('X-Csrf-Token', None)
        if not request_token or request_token != session['csrf_token']:
            abort(unauthorized("You failed to provide a valid csrf token", "csrf"))

def get_user():
    user_id = get_user_id()
    if not user_id:
        return None
    try:
        user = User.get(User.id==user_id)
        return user
    except User.DoesNotExist:
        set_user_id(None)
        return None

def get_accounts(): # list of alternative accounts as user objects
    account_ids = session.get('account_list', set())
    accounts = []
    for user_id in account_ids:
        try:
            user = User.get(User.id==user_id)
            accounts.append(user)
        except User.DoesNotExist:
            pass
    return [a.tojson() for a in accounts]

def attach_user(fn):
    @wraps(fn)
    def handler(*args, **kwargs):
        user = get_user()
        if user:
            request.user = get_user()
        return fn(*args, **kwargs)
    return handler

def require_login(fn):
    @wraps(fn)
    def handler(*args, **kwargs):
        user = get_user()
        if user:
            request.user = get_user()
            return fn(*args, **kwargs)
        else:
            abort(unauthorized("You must be logged in to view this page"))
    return handler

def require_admin(fn):
    @wraps(fn)
    def handler(*args, **kwargs):
        request.user = get_user()
        if not request.user or not request.user.is_admin:
            abort(forbidden("You don't have permission for this request."))
        return fn(*args, **kwargs)
    return handler

# -- views

@app.route("/demo", methods=["POST"])
def login_as_demo():
    demo = User.get(User.username == "demo")
    set_user_id(demo.id)
    return jsonify({"message": "You are now logged in as the demo user"})

# --- registering and login views ----

username_re = re.compile("^[a-zA-Z0-9\.\-\_]*$")
def is_valid_username(name):
    # only a-z0-9\-\.\_
    return bool(len(name) > 3 and username_re.match(name))

def is_good_password(pw):
    if len(pw) < 6:
        # XXX Note: 6 is not good enough, but I don't feel like forcing
        # people to make long passwords
        return False
    return True

email_re = re.compile("[^@]+@[^@]+\.[^@]+")
def is_valid_email(email):
    return bool(email_re.match(email))

@app.route("/register", methods=["POST"])
def register(): # signup
    user = User()

    username = request.json.get('username', '')
    password = request.json.get('password', '')
    email = request.json.get('email', '')
    name = request.json.get('name', '')

    if not is_valid_username(username):
        return bad_request("Username can only contain characters, numbers, hyphens, dots, and underscores.")

    if not is_good_password(password):
        return bad_request("Password is too weak")

    if not is_valid_email(email):
        return bad_request("Invalid email")

    # XXX TODO: send validation email message

    # Check username doesn't exist
    query = User.select().where(User.username==username)
    if query.count() != 0:
        return bad_request("Username already taken")

    user.username = username
    user.email = email
    user.name = name
    user.set_password(password)
    try:
        user.save()
        set_user_id(user.id) # login
        return jsonify({})
    except Exception, e:
        # user.save failed?
        print e
        return error_response(500, "Registration failed")

@app.route("/login", methods=["POST"])
def login():
    username = request.json['username']
    password = request.json['password']

    try:
        user = User.get(User.username==username)
    except User.DoesNotExist:
        return bad_request("Username not found")

    if not user.check_password(password):
        return bad_request("Wrong password")

    # if we got here, user authenticated correctly
    set_user_id(user.id)
    return jsonify({})

# logout requires a post request so it passes csrf check
@app.route("/logout", methods=["POST"])
def logout():
    set_user_id(None) # logout
    return jsonify({})

@app.route("/switch_account", methods=["POST"])
def switch_account():
    account_id = request.json['account_id']
    if can_switch_to(account_id):
        set_user_id(account_id) # will also renew csrf .. etc
        return jsonify({})
    else:
        return forbidden("You can't switch to that account")

