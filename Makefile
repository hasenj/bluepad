SHELL := /bin/bash
CSSDIR := static/css
LESSDIR := styles
ifdef PROD
	LESSC=lessc -x
else
	LESSC=lessc
endif

# input less files
LESSFILES=$(wildcard $(LESSDIR)/*.less)

# css files have the same name as less files but instead of being in the lessdir, they are in the cssdir
CSSFILES=$(patsubst $(LESSDIR)/%.less,$(CSSDIR)/%.css, $(LESSFILES))

all-css: $(CSSFILES)

# How to make css files from the less files
$(CSSDIR)/%.css: $(LESSDIR)/%.less
	$(LESSC) $< > $@
