# socket.io based websockets

# gevent monkey patching happens in the app module so import it first
from app import app

from flask import request, Response

from socketio import socketio_manage
from socketio.mixins import RoomsMixin
from socketio.namespace import BaseNamespace
from api import *

# based on roomsmixin
# the only difference is a classmethod broadcastor
class ChannelsMixin(object):
    sockets = {}
    def __init__(self, *args, **kwargs):
        super(ChannelsMixin, self).__init__(*args, **kwargs)
        self.sockets[id(self)] = self
        if 'channels' not in self.session:
            self.session['channels'] = set() # a set of simple strings

    def disconnect(self, *args, **kwargs):
        if id(self) in self.sockets:
            del self.sockets[id(self)]
        super(ChannelsMixin, self).disconnect(*args, **kwargs)

    def join(self, channel):
        """Lets a user join a channel on this namespace."""
        self.session['channels'].add(channel)

    def leave(self, channel):
        """Lets a user leave a channel on this namespace."""
        self.session['channels'].remove(channel)

    def on_ping(self):
        pass

    @classmethod
    def broadcast_on_channel(cls, channel, event, message):
        for socket in cls.sockets.values():
            if channel in socket.session['channels']:
                socket.emit(event, message)


class ObjectsNamespace(BaseNamespace, ChannelsMixin):
    """For sending realtime updates to things, for example, you're visiting a
    person's thoughts page and the author makes a post just now; we want the
    visitor to get the update immediately!  or, new comments arrive to a thread
    and a user is viewing that thread; the new comments should appear
    immediately!

    Updates are sent via channels. Channel names are specified using this very simple formula:

        object_type:object_id

    for example:

        thoughts:235 # here 235 is the user id
        comments:534 # here 534 is the thread id

    Clients send a message "listen" with the channel they want to listen to
    """

    def on_listen(self, channel):
        self.join(channel)
        return True

@deferred
def broadcast_update(channel, data):
    """The convention is like this:
        The message we send is a dict with two keys:

            channel: the channal name (a string)
            data: the object being sent over the channel

        The clientside javascript should deal with this as a standard
    """
    message = dict(channel=channel, data=data)
    return ObjectsNamespace.broadcast_on_channel(channel, 'update', message)

from collections import defaultdict

# XXX FIXME The sockets module should not have to depend on other modules; they
# should depend on sockets, not the other way around!
import messages

class ChatNamespace(BaseNamespace, ChannelsMixin):
    """For initiating and updating conversations

    Keeping track of who's online or offline [TODO]
    """
    # maps users (ids) to a list of their open channels
    # as long as a user has open channels, he's online
    # when a user leaves his last open channel, he's offline
    open_channels = defaultdict(list)

    def __init__(self, *args, **kwargs):
        super(ChatNamespace, self).__init__(*args, **kwargs)
        request = kwargs['request']
        if not request.user:
            # XXX do something?!
            # just give a key - this will never be really used for anything
            # just to prevent certain functions from raising exceptions
            # this is safe and doesn't interfere with garbage cleaning
            self.session['user_id'] = 'guest'
        else:
            self.session['user_id'] = request.user.id

    def user_id(self):
        return self.session['user_id']

    def user_channels(self):
        """All open channels for this user"""
        return self.open_channels[self.user_id()]

    def clear_channels(self):
        # Remove unused key from the defaultdict
        dd = self.open_channels
        key = self.user_id()

        if len(dd[key]) == 0:
            del dd[key]

    @classmethod
    def get_user_channels(cls, user_id):
        """All open channels for this user"""
        return cls.open_channels[user_id]

    # clients must send the message "online" if they want to come online,
    # otherwise we won't know that they are online
    # XXX should we make it automatic on connect? We could try overriding the
    # constructor but I noticed it's not exactly the same as connecting, so
    # this approach seems safer to me
    def on_online(self):
        was_online = self.is_online() # works because is_online depends on how many channels are connected for user
        self.user_channels().append(self)
        if True or not was_online:
            channel = 'online:{id}'.format(id=self.user_id())
            broadcast_update(channel, True)

    def disconnect(self, *args, **kwargs):
        super(ChatNamespace, self).disconnect(*args, **kwargs)
        self.user_channels().remove(self)

        if not self.is_online():
            self.clear_channels() # garbage collection
            channel = 'online:{id}'.format(id=self.user_id())
            broadcast_update(channel, False)

    def is_online(self):
        """The user is online if he has sockets connected to the chat
        namespace. Otherwise he's considered offline"""
        return len(self.user_channels()) > 0

    @classmethod
    def is_user_online(cls, user_id):
        """Class wide version of is_online"""
        return len(cls.get_user_channels(user_id)) > 0

    def order_client_open_chat(self, conversation, initiator=0):
        """Helper that sends a signal to clients to open a chat window for a conversation
        @param conversation: database Conversation object"""
        data = dict(initiator=initiator, conversation=conversation.tojson())
        event = 'open_chat'
        # tell all connected clients from this user to open a chat window for this conversation
        for socket in self.user_channels():
            socket.emit(event, data)

    @classmethod
    def broadcast_to_users(cls, user_ids, event, message):
        """Send a message to all users having their id in the user_ids list
        message is a raw dict to be sent over the socket
        event is the event name to be used"""
        for user_id in set(user_ids): # use set to remove duplicates (for chatting with oneself)
            for socket in cls.get_user_channels(user_id):
                socket.emit(event, message)

    def on_self_broadcast(self, data):
        """Re-Broadcast a message to all clients connected by this user"""
        exclude_self = data.get('_exclude_self', False)
        for socket in self.user_channels():
            if exclude_self and socket is self: continue
            socket.emit(data['event'], data['message'])

# XXX once again sockets should not need to depend on other modules!!
import follow

# This is deferred because it occurs from inside a request handler. The request
# should be able to return without depending on what happens with sockets
@deferred
def send_chat_message(message):
    """send this message to all participants in the conversation
    @param message: a database object
    """
    participant_ids = message.conversation.participant_ids()
    data = {
            'conversation_key': message.conversation.key,
            'message': message.tojson()
        }
    for pid in participant_ids:
        data['conversation_muted'] = follow.is_conversation_muted(pid, message.conversation.id)
        ChatNamespace.broadcast_to_users([pid], 'new_chat', data)

import auth

@app.route('/socket.io/<path:rest>')
def socketio(rest):
    try:
        points = {
            '/objects': ObjectsNamespace,
            '/chat': ChatNamespace
            }
        # Tell the socket about the user id
        request.user = auth.get_user()
        socketio_manage(request.environ, points, request._get_current_object())
    except:
        app.logger.error("Exception while handling socketio connection",
                         exc_info=True)
    return Response()
