# track users following and muting other users

from peewee import *

from api import *
from app import app, db_connect
from models import BaseModel, create_table
from auth import User, require_login
from messages import is_user_online, Conversation

class Following(BaseModel):
    follower = ForeignKeyField(User, related_name='follow_out')
    followed = ForeignKeyField(User, related_name='follow_in')

    @classmethod
    def by(cls, user):
        """query users followed by given `user`"""
        query = User.select()
        query = query.join(cls, on=(User.id == cls.followed))
        query = query.where(cls.follower == user)
        return query

    @classmethod
    def of(cls, user):
        """query users who follow given `user`"""
        query = User.select()
        query = query.join(cls, on=(User.id == cls.follower))
        query = query.where(cls.followed == user)
        return query

    @classmethod
    def existing(cls, by, to):
        """query existing following (if any) from `by` to `to`"""
        return cls.select().where((cls.follower == by) & (cls.followed == to))

    @classmethod
    def exists(cls, by, to):
        """Does user `by` follow user `to`?"""
        return cls.existing(by, to).count() > 0

    @classmethod
    def is_mutual(cls, user1, user2):
        """Are these 2 users both following each other?"""
        return cls.exists(user1, user2) and cls.exists(user2, user1)

    class Meta:
        # Make follower/followed pair unique so we get no duplicates
        indexes = (
            (('follower', 'followed'), True),
        )

def follow(user, target):
    existing = query_first(Following.existing(user, target))
    if existing:
        return existing
    else:
        return Following.create(follower=user, followed=target)

def unfollow(user, target):
    existing = query_first(Following.existing(user, target))
    if existing:
        existing.delete_instance()

class Muting(BaseModel):
    user = ForeignKeyField(User, related_name='mutings')
    conversation = ForeignKeyField(Conversation, related_name='mutings')

    @classmethod
    def by(cls, user):
        """query conversations muted by given `user`"""
        query = Conversation.select().join(cls)
        query = query.where(cls.user == user)
        return query

    @classmethod
    def against(cls, conversation):
        """query conversations muted by given `user`"""
        query = User.select().join(cls)
        query = query.where(cls.conversation == conversation)
        return query

    @classmethod
    def existing(cls, user, conversation):
        """Query existing muting, if any"""
        return cls.select().where((cls.user == user) & (cls.conversation == conversation))

    @classmethod
    def exists(cls, user, conversation):
        """Is there a muting by user for conersation?"""
        return cls.existing(user, conversation).count() > 0

    class Meta:
        # Make user,conversation pair unique so we get no duplicates
        indexes = (
            (('user', 'conversation'), True),
        )

def muter_ids(conversation):
    """Get a list of ids for users who have muted this conversation"""
    users = list(Muting.against(conversation))
    return [u.id for u in users]

def mute(user, conversation):
    existing = query_first(Muting.existing(user, conversation))
    if existing:
        return existing
    else:
        return Muting.create(user=user, conversation=conversation)

def unmute(user, conversation):
    existing = query_first(Muting.existing(user, conversation))
    if existing:
        existing.delete_instance()

def is_conversation_muted(user, conversation):
    existing = query_first(Muting.existing(user, conversation))
    return bool(existing)

def init():
    db_connect()
    create_table(Following)
    create_table(Muting)

init()

# ------------ view functions -------------

def get_following():
    """Returns a list of people you follow with their online/offline state"""
    if not request.user: return []
    contacts = list(Following.by(request.user))
    for c in contacts:
        c.add_json_fields(is_online=is_user_online(c))
        c.add_json_fields(is_followed=True)
    return json_list(contacts)

@app.route("/follow/<int:followed_id>", methods=["POST"])
@require_login
def follow_view(followed_id):
    f = follow(request.user.id, followed_id)
    followed = f.followed
    followed.add_json_fields(is_followed=True)
    return jsonify(followed.tojson())

@app.route("/unfollow/<int:followed_id>", methods=["POST"])
@require_login
def unfollow_view(followed_id):
    unfollow(request.user.id, followed_id)
    return jsonify({})

@app.route("/mute/<conversation_key>", methods=["POST"])
@require_login
def mute_view(conversation_key):
    conversation = Conversation.get(key=conversation_key)
    mute(request.user, conversation)
    return jsonify({})

@app.route("/unmute/<conversation_key>", methods=["POST"])
@require_login
def unmute_view(conversation_key):
    conversation = Conversation.get(key=conversation_key)
    unmute(request.user, conversation)
    return jsonify({})
