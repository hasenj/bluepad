import json

from flask import render_template, session, request, redirect, url_for, jsonify
from app import app
import auth
import thoughts
import messages
import follow
from api import *

@app.route("/")
def root():
    return splash()

@app.route("/feed", methods=["GET"])
@auth.require_login
def feed_api():
    """News feed for people you're following"""
    authors = follow.Following.by(request.user)
    query = thoughts.Thought.select().where(thoughts.Thought.author << authors)
    return jsonify_list(query)

def splash():
    user = auth.get_user()
    request.user = user
    accounts = auth.get_accounts()
    chats = messages.get_chats()
    following = follow.get_following()
    thoughts_query = thoughts.search_thoughts()
    thoughts_json = meta_objects_dict(thoughts_query)
    return render_template("pad.jn", user=user, accounts=accounts, chats=chats, thoughts=thoughts_json, following=following)

@app.route("/pad")
def pad(): # writing pad
    user = auth.get_user()
    if user is None:
        return redirect("/")
    request.user = user
    accounts = auth.get_accounts()
    chats = messages.get_chats()
    following = follow.get_following()
    thoughts_json = meta_objects_dict(thoughts.query_thoughts(user.id))
    return render_template("pad.jn", user=user, accounts=accounts, thoughts=thoughts_json, chats=chats, following=following)

@app.route("/~<author_username>")
@app.route("/~<author_username>/<thought_id>")
def author_pad(author_username, thought_id=None): # author page - lists his thoughts
    user = auth.get_user()
    request.user = user
    accounts = auth.get_accounts()
    chats = messages.get_chats()
    author = auth.User.by_username(author_username)
    if not author:
        # XXX return a special page saying the author does not exist ..
        return "Author Not Found!"
    if user:
        # add extra fields to the author object
        author.add_json_fields(is_followed=follow.Following.exists(user, author))
    following = follow.get_following()
    query = thoughts.query_thoughts(author.id)
    if thought_id: # specific thought
        print "got a thought id!"
        query = query.where(thoughts.Thought.id == thought_id)
    thoughts_json = meta_objects_dict(query)
    return render_template("pad.jn", user=user, accounts=accounts, author=author, thoughts=thoughts_json, chats=chats, following=following)
