from peewee import *

from api import *
from app import app, db_connect
from models import BaseModel
from auth import User

class Content(BaseModel):
    """This is sort of like a git commit with the blob data embedded into it.
    It will be used as the basis for everything: thoughts, comments, threads,
    etc."""
    author = ForeignKeyField(User, related_name='content')
    text = TextField()
    timestamp = DateTimeField()
    parent = ForeignKeyField('self', related_name='children', null=True) # pervious version of this "blob"

    def __repr__(self):
        return "<content({id}): {summary} by: {username} on: {time}>".format(id=self.id,
                summary=quote(truncate(self.text, 25)),
                username=self.author.username,
                time=readable_timestamp(self.timestamp))

    def fork(self, author, text): # create a new version based on this one
        return self.new(author, text, self)

    @classmethod
    def new(cls, author, text, parent=None):
        return cls.create(author=author, text=text, parent=parent, timestamp=now())

    def tojson(self):
        return dict(id=self.id, author_id=self.author.id, author=self.author.tojson(),
                text=self.text, timestamp=self.timestamp.isoformat(), parent_id=self.parent and self.parent.id or None)


# this is safe to call multiple times
def init():
    db_connect()
    if not Content.table_exists():
        print "Creating content table"
        Content.create_table()

init()
