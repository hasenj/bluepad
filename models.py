from app import app, db
from peewee import *

class BaseModel(Model):
    """A base model for all other models (specifies the database object)"""
    def __init__(self, *args, **kwargs):
        self._extra_json_fields = {}
        super(BaseModel, self).__init__(*args, **kwargs)

    def add_json_fields(self, **kwargs):
        """A way for view functions to attach extra fields"""
        self._extra_json_fields.update(kwargs)

    def basicjson(self):
        return {} # subclasses override this

    def tojson(self):
        fields = self.basicjson()
        result = {}
        result.update(self._extra_json_fields) # do this first so extra fields don't override real fields
        result.update(fields) # real fields will prevail!
        return result

    class Meta:
        database = db

def create_table(model):
    if not model.table_exists():
        print "Creating '{table}' table".format(table=model._meta.db_table)
        model.create_table()
