
function ReadingDoc(doc) {
    var self = this;
    self.title = ko.observable(doc.title);
    self.content = ko.observable(doc.content);
    self.rich_content = ko.computed(function() {
        var c, md;
        c = self.content();
        if (!c) {
            return '<em>This document has no content</em>';
        }
        md = new Showdown.converter();
        var htmlized =  md.makeHtml(c);
        return htmlized;
    });

    self.content.subscribe(function() {
        self.bidiproc();
    });

    self.bidiproc = function() {
        setTimeout(function(){
            console.log("bidi process");
            bidiweb.style('#doc *');
        },0);
    }

    self.bidiproc();

    return this;
}

function initReadingDoc(doc) {
    window.reading_doc = new ReadingDoc(doc);
}
