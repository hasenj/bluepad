MessageVM = function(data) {
    var self = this;
    self.sender = ko.observable();

    ko.mapping.fromJS(data, {}, self);

    self.html = ko.computed(function() {
        return markdown2html(self.text());
    });

    self.sender(new AuthorVM(data.sender));

    self.sender_name = ko.computed(function() {
        return self.sender().display_name();
    });

    // not from logged in user
    self.not_from_us = ko.computed(function() {
        return self.sender().id() != user().id();
    });
}

ChatVM = function(data) {
    var self = this;
    self.key = ko.observable();
    self.participants = ko.observableArray();
    self.is_muted = ko.observable(false);

    ko.mapping.fromJS(data, {}, self);

    // should the window be highlighted?
    self.highlight = ko.observable(false);

    // focus on the input field for this chat
    self.focus = ko.observable(false);

    self.participants(list_from_data(data.participants, AuthorVM));

    // get the list of participants excluding ourself but if it's a
    // conversation with ourself, do include ourself
    self.other_participants = ko.computed(function() {
        var participants = self.participants();

        // the list of participants does not include duplicates;
        // so if it's a chat with ourself, we will be the only participant
        if(participants.length == 1) { return participants; }

        var others = [];
        for(var i = 0; i < participants.length; i++) {
            var p = participants[i];
            if(p.same_as_logged_in()) {
                continue;
            }
            others.push(p);
        }
        return others;
    });

    // used when the user is scrolled up but there are new messages
    self.has_new = ko.observable(false);

    // the messages of the chat
    if(data.messages) {
        self.message_list = ko.observableArray(list_from_data(data.messages.objects, MessageVM));
        self.message_list.reverse(); // newest at the bottom
    } else {
        // allow messages to not be present (i.e. if data is just a header for chat)
        self.message_list = ko.observableArray();
    }

    // close this chat across all clients by sending a socket request
    self.close = function() {
        sockets.request_close_chat(self.key())
    }

    var alerter = 0;

    // play alert sound, flash title, and highlight chat pane
    self.notify_and_alert = function() {
        self.highlight(true);
        chat_notifier.play();
        titleFlasher.flash("chat ...");
    }

    // stop flashing title and remove the highlight from chat pane
    self.stop_alerting = function() {
        clearTimeout(alerter); // stop the ongoing alerting attempt - if any
        titleFlasher.stop();
        // delay removing the highlight a couple seconds
        setTimeout(function() {
            self.highlight(false);
        }, 1900);
    }

    // if user is not on page, notify & alert
    self.demand_attention = function() {
        if(self.is_muted()) {
            return; // muted conversations don't demand attention
        }
        if(!pageFocus.checkFocus()) {
            // wait a bit before starting to alert, give some chance to other
            // clients to indicate they've read the message
            var delay = 800;
            alerter = setTimeout(function() {
                self.notify_and_alert();
                // listen for page coming to focus
                var sub = pageFocus.pageFocused.subscribe(function(focused) {
                    if(focused) {
                        sub.dispose();
                        self.stop_alerting();
                        // tell other clients to also stop alerting!
                        sockets.stop_chat_alert(self.key());
                    }
                });
            }, delay);
        } else {
            // we are focused here .. so don't let other clients "stray" and demand attention because none is needed!
            sockets.stop_chat_alert(self.key());
        }
    }

    self.push_message = function(message) {
        self.message_list.push(message);

        // if this message is not from us, and the window is not focused, start
        // notification and alerting
        if(message.not_from_us()) {
            self.demand_attention();
        }
    }

    // listen to new messages
    sockets.chat_listener(self.key(), function(message_data) {
        self.push_message(new MessageVM(message_data));
    })

    // helper for scroll position
    var distanceToBottom = function(element) {
        return element.scrollHeight - element.scrollTop - element.clientHeight;
    }

    // called when the messages pane get scrolled
    self.messages_scrolled = function(data, event) {
        var container = event.target; // assuming this is the messages div
        // almost at the bottom? then consider everything is read: close
        // notification about unread messages
        if(distanceToBottom(container) < 3) {
            self.has_new(false);
        }
    }

    self.chat_message_after_render = function(elements, data_item) {
        // helper; not general purpose
        var chatHeight = function(elements) {
            var total = 0;
            for(var i = 0; i < elements.length; i++) {
                var e = elements[i];
                var h = e.clientHeight;
                if(h) {
                    total += h;
                }
            }
            return total;
        }
        var container = elements[0].parentNode;
        var insertedHeight = chatHeight(elements)
        var tolerance = 20;
        if(distanceToBottom(container) <= insertedHeight + tolerance) { // already scrolled to bottom? force scroll down
            container.scrollTop = container.scrollHeight - container.clientHeight; // scrolls to bottom
        } else {
            // the user has scrolled up - don't force the chat to scroll down,
            // instead show a notification about new messages
            self.has_new(true);
        }
    }

    // input for sending chat messages
    self.submittingMessage = ko.observable(false);
    self.inputMessage = ko.observable('');
    self.submit = function() {
        self.submittingMessage(true)
        var text = self.inputMessage().trim();
        if(!text) { return; }
        var url = "/conversation/" + self.key();
        var data = {text: text};
        requests.post(url).data(data).
            success(function(response) {
                self.inputMessage('');
                // don't add the message - wait for it to arrive via socket
            }).error(function(response) {
                alert(response.error);
            }).complete(function() {
                self.submittingMessage(false);
            }).send();
    }


    // -------------------------------------------------------------------------
    // ------------------ Settings menu ----------------------------------------

    self.settings_menu_visible = ko.observable(false);
    self.toggle_settings_menu = function() {
        if(self.settings_menu_visible()) { return; } // nothing to do - event handler will take care of it

        // show menu and set it to hide on clicking document
        self.settings_menu_visible(true);
        falseOnDocumentClick(self.settings_menu_visible);
    }

    self.mute = function() {
        var url = "/mute/" + self.key();
        requests.post(url).
            success(function() {
                self.is_muted(true);
                // XXX broadcast mute instead of close ..
                sockets.request_close_chat(self.key())
            }).send();
    }

    self.unmute = function() {
        var url = "/unmute/" + self.key();
        requests.post(url).
            success(function() {
                self.is_muted(true);
                // XXX broadcast unmute
            }).send();
    }

    self.clear = function() {
        self.message_list.removeAll();
    }
}

open_chats = ko.observableArray();

init_chats = function(chats_data) {
    open_chats(list_from_data(chats_data, ChatVM));
}

chat_key_with = function(other_user_id) {
    var self_id = user().id();
    return [self_id, other_user_id].sort().join('|')
}

find_chat = function(key) {
    return open_chats().find(function(c){ return c.key() == key; });
}

/**
    Open conversation by key
    @param f_focus: (optional) should we focus on the chat after opening it?
 */
open_chat_with = function(other_user_id, f_focus) {
    var focus = function(chat) {
        if(f_focus) {
            chat.focus(true);
        }
    }
    var key = chat_key_with(other_user_id);
    var chat = find_chat(key);
    if(chat) { // chat already open? just focus it
        focus(chat);
    } else { // request server to open the chat, then rebroadcast to all other clients to also open that chat
        var url = "/conversation_with/" + other_user_id;
        requests.get(url).success(function(data) {
                var conversation_data = data;
                // check again that the chat isn't open .. (because this callback is called alter)
                var chat = find_chat(key);
                if(chat) {
                    focus(chat);
                } else { // good, use this chat data to create the chat
                    var chat = new ChatVM(conversation_data);
                    open_chats.push(chat);
                    focus(chat);
                    var rebroadcast_data = { conversation: conversation_data, initiator: user().id() }
                    sockets.self_broadcast_others('open_chat', rebroadcast_data);
                }
            }).error(function(response) {
                alert(response); // XXX What???
            }).send();
    }
}

chat_from_incoming_message = function(data) {
    var message = new MessageVM(data.message);
    // construct chat data from the incoming data
    var chat_data = {
        key: data.conversation_key,
        participants: Array.create(data.message.recipients, data.message.sender),
        is_muted: data.conversation_muted
    }
    var chat = new ChatVM(chat_data);
    open_chats.push(chat);
    chat.push_message(message);
}

BuddyList = function() {
    var self = this;
    self.buddies = ko.observableArray();

    self.is_empty = ko.computed(function() {
        return self.buddies().length == 0;
    });

    self.is_minimized = ko.observable(false);

    self.toggle_minimize = function() {
        ko_toggle_boolean(self.is_minimized)
    }

    // this function prevents adding duplicate buddies
    self.add_buddy_data = function(buddy_data) { // buddy data is author data
        var existing = self.buddies().find(function(b) { return b.id() == buddy_data.id; });

        if(existing) { return; }
        self.buddies.push(new AuthorVM(buddy_data));
    }

    self.remove_buddy_by_id = function(buddy_id) {
        self.buddies.remove(function(b){ return b.id() == buddy_id; });
    }
}

buddy_list = new BuddyList()

init_buddies = function(buddies_data) {
    buddy_list.buddies(list_from_data(buddies_data, AuthorVM));
}


// -----------------------------------------------------
// ------- Notification helpers ------------------------
// -----------------------------------------------------

TitleFlasher = function() {
    var self = this;
    self.originalTitle = document.title;

    self.alt = [];
    self.index = 0;
    self.timer = 0;

    self.flash = function(text) {
        self.stop(); // stop any ongoing flasher .. (if any exists)

        // remember the original title to be restored when we stop
        self.originalTitle = document.title;

        self.alt = [text, self.originalTitle];
        self.timer = setTimeout(self.reflash, 0);
    }

    self.reflash = function() {
        document.title = self.alt.at(self.index);
        self.index++;
        self.timer = setTimeout(self.reflash, 1400);
    }

    self.stop = function() {
        document.title = self.originalTitle;
        self.alt = [];
        self.index = 0;
        clearTimeout(self.timer);
    }
}

titleFlasher = new TitleFlasher();

chat_notifier = new Audio("static/sounds/chat_notification.mp3");
if(!chat_notifier.canPlayType("audio/mpeg")) {
    chat_notifier = new Audio("static/sounds/chat_notification.ogg");
}
