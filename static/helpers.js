/**
    parse markdown with a sanitizer and a bidi processor
 */
markdown2html = function(text) {
    var converter = Markdown.getSanitizingConverter();
    var html = converter.makeHtml(text);
    return bidiweb.html_css(html);
}

reload_root = function() {
    // reload the page at the root to force renew everything
    // useful for logging in and out, etc
    location.href = '/';
}

auto_clear = function(ob, wait) {
    if(!wait) {
        wait = 6200;
    }
    var timer = 0;
    var clear = function() {
        ob('');
    }
    ob.subscribe(function(val) {
        if(timer) {
            clearTimeout(timer);
        }
        timer = setTimeout(clear, wait);
    });
}

// auto-cleared observable
// assume string only for now, until another use case appears
auto_cleared_observable = function(initial_value, wait) {
    if(!initial_value) { initial_value = ''; }
    var ob = ko.observable(initial_value);
    auto_clear(ob, wait);
    return ob;
}

ko_toggle_boolean = function(ob) {
    ob(!ob());
}

// simulate an event ..
now_timestamp = function() {
    return Date.utc.create().toISOString();
}

list_from_data = function(data, itemClass) {
    var items = [];
    for(var i = 0; i < data.length; i++) {
        items.push(new itemClass(data[i]));
    }
    return items;
}

// push an array to an observable array
push_array = function(obArray, otherArray) {
    for(var i = 0; i < otherArray.length; i++) {
        obArray.push(otherArray[i]);
    }
}

// unshift an array to an observable array
unshift_array = function(obArray, otherArray) {
    for(var i = 0; i < otherArray.length; i++) {
        obArray.unshift(otherArray[i]);
    }
}

/**
    Utility for detecting when the user leaves this page and comes back to it

    There's a visibility API but it only seems to work when the user switches to another tab within the browser! It does not work when the user leaves the browser completely!

    On the other hand, there's a window focus and blur events, but they don't seem to work when the user switches to another tab within the current browser.

    So, this utility object brings these two mechanisms together to provide a uniform streamlined API for detecting whether the user is viewing this page or not.
 */
PageFocus = function() {
    var self = this;

    // from the mozilla wiki
    // Set the name of the hidden property and the change event for visibility
    var hidden, visibilityChange;
    if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support
        hidden = "hidden";
        visibilityChange = "visibilitychange";
    } else if (typeof document.mozHidden !== "undefined") {
        hidden = "mozHidden";
        visibilityChange = "mozvisibilitychange";
    } else if (typeof document.msHidden !== "undefined") {
        hidden = "msHidden";
        visibilityChange = "msvisibilitychange";
    } else if (typeof document.webkitHidden !== "undefined") {
        hidden = "webkitHidden";
        visibilityChange = "webkitvisibilitychange";
    }

    self.pageFocused = ko.observable();

    self.checkFocus = function() {
        var docFocused = document.hasFocus();
        var pageVisible = !document[hidden];

        // update our main flag
        self.pageFocused(pageVisible && docFocused);
        return self.pageFocused();
    }

    document.addEventListener(visibilityChange, self.checkFocus);
    window.addEventListener("focus", self.checkFocus);
    window.addEventListener("blur", self.checkFocus);

    document.onreadystatechange = self.checkFocus;
}

pageFocus = new PageFocus();

// ------------------------------

// From
// http://blog.pardahlman.se/2012/10/tooltip-from-twitter-bootstrap-with-knockout-js-observable/
ko.bindingHandlers.tooltip = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        var valueUnwrapped = ko.utils.unwrapObservable(valueAccessor());
        // requires bootstrap's tooltip
        $(element).tooltip(valueUnwrapped);
    },
};

// ------------------------------

// menu toggling helpers
ko.bindingHandlers.popup_toggler = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        // This will be called when the binding is first applied to an element
        // Set up any initial state, event handlers, etc. here

        setTimeout(function() {
            // just set it up so clicking anywhere outside menu hides it
            element.addEventListener('click', function(event) {
                // XXX TODO: exclude a certain class .. so clicking on it also hides menu
                event.stopPropagation();
            });
            document.addEventListener('click', function() {
                var visible = valueAccessor();
                visible(false);
            });
        }, 0);
    },
    update: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        // This will be called once when the binding is first applied to an element,
        // and again whenever the associated observable changes value.
        // Update the DOM element based on the supplied values here.
    }
};


// an alternative menu toggling helper
// set the observable to false when any click event occurs on the document
var falseOnDocumentClick = function(ob) {
    var hideIt = function() {
        if(ob()) {
            ob(false);
        }
    }

    var eventHandler = function() {
        setTimeout(function() {
            hideIt();
            document.removeEventListener('click', eventHandler);
        }, 0);
    }

    setTimeout(function() {
        document.addEventListener('click', eventHandler);
    }, 100);
}


// -----------------------------------------------------
// ------ custom knockout bindings ---------------------
// -----------------------------------------------------

// expandable input fields
// automatically expand height to reach
ko.bindingHandlers.autoHeight = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        // This will be called when the binding is first applied to an element
        // Set up any initial state, event handlers, etc. here

        var $el = $(element);
        // inner height is height + padding
        // outerHeight includes border (and possibly margins too?)
        var padding = $el.innerHeight() - $el.height();
        var originalHeight = $el.height();

        var adjust = function() {
            // reset the original height so that scrollHeight makes more sense
            // (see below)
            $el.height(originalHeight);
            var maxHeight = valueAccessor();
            // scrollHeight is max of current height and content height.
            // i.e. if content height is less than current height, it will
            // return curent height.
            // but, scrollHeight includes padding so we need to subtract it
            var height = element.scrollHeight - padding;
            height = Math.min(height, maxHeight);
            // don't go smaller than original
            height = Math.max(height, originalHeight);
            element.style.height = height + "px";
        }.throttle(50);

        element.addEventListener('input', adjust);

        var allBindings = allBindingsAccessor()
        if(allBindings.value) {
            allBindings.value.subscribe(function() {
                adjust.delay(0);
            });
        }

        adjust.delay(0);
    },

    update: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        // This will be called once when the binding is first applied to an element,
        // and again whenever the associated observable changes value.
        // Update the DOM element based on the supplied values here.
    }
};

ko.bindingHandlers.autoBidi = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        // This will be called when the binding is first applied to an element
        // Set up any initial state, event handlers, etc. here

        var restyle = function() {
            bidiweb.style(element);
        }.throttle(50);

        element.addEventListener('input', function() {
            restyle();
        });

        var allBindings = allBindingsAccessor()
        if(allBindings.value) {
            allBindings.value.subscribe(function() {
                restyle.delay(0);
            });
        }

        restyle.delay(0);
    },

    update: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        // This will be called once when the binding is first applied to an element,
        // and again whenever the associated observable changes value.
        // Update the DOM element based on the supplied values here.
    }
};

ko.bindingHandlers.submitOnEnter = {
    init: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        // This will be called when the binding is first applied to an element
        // Set up any initial state, event handlers, etc. here

        // listen keypress so we can do preventDefault to prevnt a new line
        // from being inserted if we decide to submit
        element.addEventListener('keypress', function(e) {
            if (e.keyCode === 13 && !e.ctrlKey) {
                e.preventDefault();
                $(element).parent('form').submit();
            }
            return true;
        });
    },

    update: function(element, valueAccessor, allBindingsAccessor, viewModel, bindingContext) {
        // This will be called once when the binding is first applied to an element,
        // and again whenever the associated observable changes value.
        // Update the DOM element based on the supplied values here.
    }
};
