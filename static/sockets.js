sockets = function(){
    var sockets = {};
    sockets.channels = {};
    sockets.listen = function(channel, callback) {
        if(!(channel in sockets.channels)) {
            sockets.channels[channel] = [];
        }
        sockets.channels[channel].push(callback);

        // join the channel
        // XXX should we worry about already being on the channel?!
        if(sockets.socket && sockets.connected) {
            sockets.socket.emit('listen', channel);
        }
    }

    sockets.socket = null;
    sockets.connected = false;

    // the objects socket
    $(document).ready(function() {
        var socket = io.connect('/objects');
        socket.on('connect', function() {
            sockets.connected = true;

            // join all channels we're interested in
            for(channel in sockets.channels) {
                socket.emit('listen', channel);
            }
        });

        socket.on('disconnect', function() {
            sockets.connected = false;
        });

        socket.on('update', function(message) {
            // console.log("Got update data:", message);
            var data = message.data;
            var channel = message.channel;
            var callbacks = sockets.channels[channel] || [];
            for(var index = 0; index < callbacks.length; index++) {
                try {
                    callbacks[index](data);
                } catch(e) {
                    console.log("Callback error. Channel:", channel);
                    console.error(e.stack);
                }
            }
        });

        // ping all the time .. for nginx timeout
        var ping = function() {
            socket.emit('ping');
            setTimeout(ping, 30 * 1000);
        }
        ping();

        sockets.socket = socket;
    });

    var chat_listeners = {};
    sockets.chat_listener = function(conversation_key, callback) {
        if(!(conversation_key in chat_listeners)) {
            chat_listeners[conversation_key] = [];
        }
        chat_listeners[conversation_key].push(callback);
    }


    // the chat socket
    // the objects socket
    $(document).ready(function() {
        var chatSocket = io.connect('/chat');
        chatSocket.on('connect', function() {
            chatSocket.emit('online');
        });

        chatSocket.on('open_chat', function(data) {
            // the message here is the conversation data
            if(!(data.conversation.key in chat_listeners)) {
                var chat = new ChatVM(data.conversation)
                // if chat is muted and this opening was not initiated by us,
                // do nothing!
                // This should never happen because we don't request opening
                // chats when we receive a new message on a muted conversatiom,
                // but it's always a good idea to double check, to deal with
                // corner cases (e.g. related to timing)
                if(data.initiator != user().id() && chat.is_muted()) {
                    return;
                }
                open_chats.push(chat);
                requests.post("/add_conversation/" + data.conversation.key).send();
                // if the chat opened because someone (other than us) sent us a
                // message, demand user attention
                if(data.initiator != user().id()) {
                    chat.demand_attention();
                }
            }
        });

        chatSocket.on('close_chat', function(key) {
            if(key in chat_listeners) {
                delete chat_listeners[key];
                var removed_chats = open_chats.remove(function(item) { return item.key() == key; });
                // XXX do we need to clean garbage after removing the item?!
                requests.post("/remove_conversation/" + key).send();
            }
        });

        // XXX this should be the responsibility of chat.js
        chatSocket.on('mute_chat', function(key) {
            if(key in chat_listeners) {
                var chat = open_chats().find(function(item) { return item.key() == key; });
                if(!chat) return;
                chat.is_muted(true);
            }
        });

        // XXX this should be the responsibility of chat.js
        chatSocket.on('unmute_chat', function(key) {
            if(key in chat_listeners) {
                var chat = open_chats().find(function(item) { return item.key() == key; });
                if(!chat) return;
                chat.is_muted(false);
            }
        });


        // request the server to broadcast message to all connected clients ..
        sockets.self_broadcast = function(event, message) {
            chatSocket.emit('self_broadcast', {event: event, message: message})
        }

        // rebroadcast a message to all clients except this one
        sockets.self_broadcast_others = function(event, message) {
            chatSocket.emit('self_broadcast', {event: event, message: message, _exclude_self: true})
        }


        sockets.request_close_chat = function(key) {
            sockets.self_broadcast('close_chat', key);
        }

        sockets.stop_chat_alert = function(key) {
            sockets.self_broadcast_others('stop_chat_alert', key);
        }

        // XXX this should be the responsibility of chat.js
        // we should have a mechanism where we just redirect the message to the chat object and it has to deal with it itself!
        chatSocket.on('stop_chat_alert', function(key) {
            var chat = open_chats().find(function(item) { return item.key() == key; });
            if(!chat) return;
            chat.stop_alerting();
        });

        // XXX this should be the responsibility of chat.js
        chatSocket.on('new_chat', function(data) {
            // console.log("Got new chat:", data);
            // see messages.py#send_chat_message for data structure
            var key = data.conversation_key;
            var message = data.message;
            var callbacks = chat_listeners[key] || [];
            for(var i = 0; i < callbacks.length; i++) {
                try {
                    callbacks[i](message);
                } catch(e) {
                    console.log("Callback error. Conversation:", key);
                    console.error(e.stack);
                }
            }

            // if no one is listening for updates to this convo, then the chat is not open
            // so we need to open it!
            if(callbacks.length == 0) {
                if(!data.conversation_muted) {
                    chat_from_incoming_message(data);
                }
            }
        });

        // XXX this should *really* be moved to chat.js
        chatSocket.on('started_following', function(author_data) {
            buddy_list.add_buddy_data(author_data);
            if(author() && author().id() == author_data.id) {
                author().is_followed(true);
            }
        });

        // XXX this should *really* be moved to chat.js
        chatSocket.on('stopped_following', function(author_id) {
            buddy_list.remove_buddy_by_id(author_id);
            if(author() && author().id() == author_id) {
                author().is_followed(false);
            }
        });



        // ping all the time .. for nginx timeout
        var ping = function() {
            chatSocket.emit('ping');
            setTimeout(ping, 30 * 1000);
        }
        ping();
    });

    return sockets;
}()
