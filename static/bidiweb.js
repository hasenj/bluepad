
bidi={}
bidi.Dir={RTL:-1,UNKNOWN:0,LTR:1};bidi.Format={LRE:'\u202A',RLE:'\u202B',PDF:'\u202C',LRM:'\u200E',RLM:'\u200F'};bidi.ltrChars_='A-Za-z\u00C0-\u00D6\u00D8-\u00F6\u00F8-\u02B8\u0300-\u0590\u0800-\u1FFF'+'\u2C00-\uFB1C\uFE00-\uFE6F\uFEFD-\uFFFF';bidi.rtlChars_='\u0591-\u07FF\uFB1D-\uFDFF\uFE70-\uFEFC';bidi.ltrDirCheckRe_=new RegExp('^[^'+bidi.rtlChars_+']*['+bidi.ltrChars_+']');bidi.ltrCharReg_=new RegExp('['+bidi.ltrChars_+']');bidi.hasAnyLtr=function(text){return bidi.ltrCharReg_.test(text);};bidi.rtlDirCheckRe_=new RegExp('^[^'+bidi.ltrChars_+']*['+bidi.rtlChars_+']');bidi.rtlRe=bidi.rtlDirCheckRe_;bidi.isRtlText=function(text){return bidi.rtlDirCheckRe_.test(text);}
bidi.isLtrText=function(text){return bidi.ltrDirCheckRe_.test(text);}
bidi.isRequiredLtrRe_=/^http:\/\/.*/;bidi.hasNumeralsRe_=/\d/;bidi.estimateDirection=function(text,detectionThreshold){var rtlCount=0;var totalCount=0;var hasWeaklyLtr=false;var tokens=text.split(/\s+/);for(var i=0;i<tokens.length;i++){var token=tokens[i];if(bidi.isRtlText(token)){rtlCount++;totalCount++;}else if(bidi.isRequiredLtrRe_.test(token)){hasWeaklyLtr=true;}else if(bidi.hasAnyLtr(token)){totalCount++;}else if(bidi.hasNumeralsRe_.test(token)){hasWeaklyLtr=true;}}
return totalCount==0?(hasWeaklyLtr?bidi.Dir.LTR:bidi.Dir.UNKNOWN):(rtlCount/totalCount>detectionThreshold?bidi.Dir.RTL:bidi.Dir.LTR);};bidiweb=(function(){var module={};IProcessor={makeRtl:function(element){},makeLtr:function(element){}}
var css_processor=function(classes){return{makeRtl:function(element){element.classList.add(classes.rtl);},makeLtr:function(element){element.classList.add(classes.ltr);}}}
var style_processor=function(falign){return{makeRtl:function(element){element.style.direction="rtl";if(falign){element.style.textAlign="right";}},makeLtr:function(element){element.style.direction="ltr";if(falign){element.style.textAlign="left";}}}}
module.processors={css:css_processor,style:style_processor}
var nodeListMock=function(node){var list=[node];list.item=function(i){return list[i];}
return list;}
module.process=function(query,processor){var elements;if(query instanceof NodeList){elements=query;}else if(query instanceof Node){elements=nodeListMock(query);}else{elements=document.querySelectorAll(query);}
module.process_elements(elements,processor);return elements;}
module.process_elements=function(elements,processor){for(var index=0;index<elements.length;index++){var element=elements.item(index);var text=element.textContent||element.value||element.placeholder||"";var dir=bidi.estimateDirection(text,0.4);if(dir==bidi.Dir.RTL){processor.makeRtl(element);}else if(dir==bidi.Dir.LTR){processor.makeLtr(element);}};}
module.process_css=function(query,classes){var proc=module.processors.css(classes);return module.process(query,proc);}
module.process_style=function(query,falign){var proc=module.processors.style(falign);return module.process(query,proc);}
module.style=function(query){return module.process_style(query,true);}
module.css=function(query){return module.process_css(query,{rtl:'rtl',ltr:'ltr'});}
module.htmlToElement=function(html){var container=document.createElement('div');container.innerHTML=html;return container}
module.html_css=function(html){var container=module.htmlToElement(html);var nodes=container.querySelectorAll('*');module.css(nodes);return container.innerHTML;}
module.html_style=function(html){var container=module.htmlToElement(html);var nodes=container.querySelectorAll('*');module.style(nodes);return container.innerHTML;}
return module;})();