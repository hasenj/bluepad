ThoughtForm = function() {
    var self = this;

    self.id = ko.observable(null);

    self.text = ko.observable("");

    // for preview
    self.html = ko.computed(function() {
        return markdown2html(self.text());
    });

    self.clear = function() {
        self.text("");
    }

    self.error_message = auto_cleared_observable();

    self.sending = ko.observable(false);
    self.submit_enabled = ko.computed(function() {
        return self.text() && !self.sending();
    });

    self.url = ko.computed(function() {
        if(self.id()) {
            return "/thought/" + self.id();
        } else {
            return "/thought/";
        }
    });
    self.requestMethod = ko.computed(function() {
        if(self.id()) {
            return "PATCH";
        } else {
            return "POST";
        }
    });

    // use to trigger events when we make a successful post/submit/save
    self.posted = ko.observable();
    self.cancelled = ko.observable();

    self.submit = function() {
        self.sending(true);
        requests.request(self.requestMethod(), self.url()).
            data({text: self.text()}).
            success(function(response) {
                console.log("Posted sucessfully");
                self.posted(now_timestamp()); // trigger an event
            }).error(function(response) {
            }).complete(function(){
                self.sending(false);
            }).send();
    }

    self.mode = ko.observable('edit'); // edit|preview
    self.enterPreviewMode = function() {
        self.mode('preview');
    }
    self.exitPreviewMode = function() {
        self.mode('edit');
    }
    self.inEditMode = ko.computed(function() {
        return self.mode() == 'edit';
    });
    self.inPreviewMode = ko.computed(function() {
        return self.mode() == 'preview';
    });

    self.cancel = function() {
        self.cancelled(now_timestamp()); // trigger an event
    }
}

thought_form = ko.observable(null);

// create top most form for posting new thoughts
var makePostThoughtForm = function() {
    var form = new ThoughtForm();
    form.posted.subscribe(function() {
        // clear this form ..
        form.clear();
        // Note: Don't add the new thought: it will arrive via the push socket
    });
    form.cancelled.subscribe(function() {
        form.clear();
        form.exitPreviewMode();
    });
    return form;
}

ThoughtVM = function(data) {
    var self = this;

    self.id = ko.observable();
    self.text = ko.observable('');
    self.timestamp = ko.observable();

    ko.mapping.fromJS(data, {}, self);

    self.html = ko.computed(function() {
        return markdown2html(self.text());
    });

    self.date_display = ko.computed(function() {
        var date = Date.utc.create(self.timestamp());
        return date.format("short") + " (" + date.relative() + ") ";
    });

    self.author_name = ko.computed(function() {
        return self.author.name() || self.author.username();
    });

    self.author_page_url = ko.computed(function() {
        return author_page_url(self.author);
    });

    self.permalink = ko.computed(function() {
        return "/~" + self.author.username() + "/" + self.id();
    });

    // --------- edit and delete controls --------

    // is this thought authored by the logged in user?
    self.byUser = ko.computed(function() {
        if(!user()) { return false; }
        return self.author_id() == user().id();
    });

    self.controlsVisible = ko.observable(false);

    self.showControls = function() {
        if(self.byUser()) {
            self.controlsVisible(true);
        }
    }

    self.hideControls = function() {
        self.controlsVisible(false);
    }

    self.highlighted = ko.observable(false);
    self.deleteThought = function() {
        self.highlighted(true);
        if(confirm("You're about to delete the highlighted thought! " +
                    "Are you sure you want to do this?\n\n" +
                    "You can't undo this if you change your mind later!\n\n" +
                    "If you're sure, press OK to delete.")) {
            var url = "/thought/" + self.id();
            requests.del(url).success(function() {
                // XXX display some animation?!
                pad().thought_list.remove(self);
                // XXX cleanup? close chat listeners? any resources we need to free?
            }).error(function(response) {
                alert(response.error);
            }).complete(function() {
                self.highlighted(false);
            }).send();
        }
        self.highlighted(false);
    }

    self.mode = ko.observable('view');
    self.editForm = ko.observable(null);
    self.enterEditMode = function() {
        var form = new ThoughtForm();
        form.id(self.id());
        form.text(self.text());
        self.editForm(form);

        // close the form when it successfully posts its content
        var sub = self.editForm().posted.subscribe(function() {
            sub.dispose();
            self.text(form.text());
            self.exitEditMode();
        });

        var sub2 = self.editForm().cancelled.subscribe(function() {
            sub2.dispose();
            self.exitEditMode();
        });

        // NOTE: must come after setting editForm, or the template will die
        self.mode('edit');
    }
    self.exitEditMode = function() {
        self.mode('view');
        // NOTE: must come after exiting edit mode, or the template will die
        self.editForm(null);
    }

    // -------- comments & commenting stuff ------
    self.comment_form = ko.computed(function() {
        if(user()) {
            return new ThoughtCommentForm(self);
        } else {
            return null;
        }
    });

    self.thread = new ThreadVM(data.thread_id, data.comments);

    // --------- UX tricks ----------
    self.is_new = ko.observable(false);

    self.is_new.subscribe(function(val) {
        if(val) {
            // if is_new is set to true, set it to false after a couple minutes
            setTimeout(function() {
                self.is_new(false);
            }, 3 * 60 * 1000);
        }
    });
}

var Pad = function(author_id, thoughts_data) {
    var self = this;

    self.thought_list = ko.observableArray();
    self.count = ko.observable(0);

    self.has_more = ko.computed(function() {
        return self.count() > self.thought_list().length;
    });

    var vm_list = list_from_data(thoughts_data.objects, ThoughtVM)
    self.thought_list(vm_list);
    self.count(thoughts_data.meta.count);

    $(document).ready(function() {
        // XXX this is not compatible with global posts feed
        var channel = "thoughts:" + author_id;
        sockets.listen(channel, function(data) {
            var thought = new ThoughtVM(data);
            thought.is_new(true);
            self.thought_list.unshift(thought);
        });

    });

    self.loading_more = ko.observable(false);

    // XXX this loading mechanism is not compatible with global posts feed
    self.load_more = function() {
        var url = "/thought";
        var params = {
            before: self.thought_list().last().id(),
            author_id: author_id
        };
        requests.get(url).params(params).
            success(function(response) {
                var new_items = list_from_data(response.objects, ThoughtVM)
                for(var i = 0; i < new_items.length; i++) {
                    self.thought_list.push(new_items[i]);
                }
            }).error(function(response) {
            }).complete(function() {
                self.loading_more(false);
            }).send();
        self.loading_more(true);
    }

}

pad = ko.observable(null);

init_thoughts = function(author_id, data) {
    pad(new Pad(author_id, data));
}

thought_after_add = function(element, index, thought_vm) {
    if(element.nodeType == element.TEXT_NODE) { return; }
    // not sure if the element here is the thought box or a parent of it
    var box = element.querySelector('.thought_box') || element;
    hide_thought(box, thought_vm);
    show_thought(box, thought_vm);
}

var show_thought = function(element, thought_vm) {
    element.style.transition = '800ms';
    var height = element.querySelector('.thought').clientHeight;
    element.style.height = height + 'px'; // assuming 'element' is the .thought_box
    element.classList.remove('hidden');
    setTimeout(function() {
        element.style.transition = '0s';
        element.style.height = "auto";
    }, 900);
}

var hide_thought = function(element, thought_vm) {
    element.style.transition = '0s';
    element.style.height = '0px';
    element.classList.add('hidden');
}

author = ko.observable(null);

// param: thought: a ThoughtVM instance
ThoughtCommentForm = function(thought) {
    var self = this;

    self.text = ko.observable('');

    self.clear = function() {
        self.text("");
    }

    self.trimmed_text = ko.computed(function() {
        return self.text().trim();
    });

    self.error_message = auto_cleared_observable();

    self.sending = ko.observable(false);
    self.submit_enabled = ko.computed(function() {
        return self.trimmed_text() && !self.sending();
    });

    self.submit = function() {
        if(!self.submit_enabled()) { return; }
        var url = "/thought/" + thought.id() + "/comment"
        self.sending(true);
        requests.post(url).data({text: self.trimmed_text()}).
            complete(function() {
                self.sending(false);
            }).success(function(response) {
                self.clear();
            }).error(function(response){
                self.error_message(response.error);
            }).send();
    }
}

var SearchForm = function() {
    var self = this;
    self.active_term = ko.observable(''); // what's already searched for
    self.search_term = ko.observable(''); // what the user is typing
    self.working = ko.observable(false);

    self.go = function() {
        var url = "/search";
        var params = {
            term: self.search_term()
        }
        requests.get(url).params(params).
            success(function(response) {
                self.active_term(params.term);
                var items = list_from_data(response.objects, ThoughtVM)
                pad().thought_list(items);
                pad().count(response.meta.count);

            }).error(function(response) {
            }).complete(function() {
                self.working(false);
            }).send();
        self.working(true);
    }

    self.enabled = ko.computed(function() {
        return !self.working();
    });
}

search_form = ko.observable(null);



// -------------------------------------------------------
// ---------  Comments and Threads  ----------------------
// -------------------------------------------------------

CommentVM = function(data) {
    var self = this;

    self.text = ko.observable('');

    ko.mapping.fromJS(data, {}, self);

    self.html = ko.computed(function() {
        return markdown2html(self.text()) || "&nbsp;";
    });

    self.author_page_url = ko.computed(function() {
        return author_page_url(self.author);
    });

    // TODO: extract author to its own VM pseudo-class
    self.author_name = ko.computed(function() {
        return self.author.name() || self.author.username();
    });

}

ThreadVM = function(thread_id, comments_data) {
    var self = this;
    self.thread_id = ko.observable(thread_id); // it won't change but for the sake of consistency all ids are observables ..

    self.comments = ko.observableArray(list_from_data(comments_data.objects, CommentVM).reverse());
    self.comments_count = ko.observable(comments_data.meta.count);

    // how many past comments are not shown?
    // this is not a computed because the comments_count does not update when new comments arrive via sockets
    // so we need to update it manually in the load_more_comments function
    self.not_shown_comments_count = ko.observable(comments_data.meta.count - comments_data.objects.length);

    self.has_more_comments = ko.computed(function() {
        return self.comments_count() > self.comments().length;
    });

    self.loading_more_comments = ko.observable(false);
    self.load_more_comments = function() {
        if(self.loading_more_comments()) { return; }
        var url = "/thread/" + thread_id + "/comment";
        var params = {
            before: self.comments().first().id()
        };
        requests.get(url).params(params).
            success(function(response) {
                // update the count of unshown comments
                self.not_shown_comments_count(self.not_shown_comments_count() - response.objects.length);
                var new_comments = list_from_data(response.objects, CommentVM);
                new_comments.forEach(function(c) {
                    self.comments.unshift(c);
                });
            }).error(function(response) {
            }).complete(function() {
                self.loading_more_comments(false);
            }).send();
        self.loading_more_comments(true);
    }

    // add comments as they arrive via web sockets
    var update_channel = "comments:" + thread_id;
    sockets.listen(update_channel, function(data) {
        self.comments.push(new CommentVM(data));
    });
}
