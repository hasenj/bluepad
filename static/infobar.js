var GlobalBinding;

logout = function() {
    // TODO: loading gif ..?
    requests.post("/logout").success(function() {
        location.href = "/";
    }).send();
};

author_page_url = function(user) {
    return "/~" + user.username();
}

var UserVM = function(data) {
    var self = this;
    self.name = ko.observable();
    self.username = ko.observable();

    ko.mapping.fromJS(data, {}, self);

    self.display_name = ko.computed(function() {
        return self.name() || self.username();
    });

    self.author_page_url = ko.computed(function() {
        return author_page_url(self);
    });

    self.full_author_page_url = ko.computed(function() {
        return location.protocol + "//" + location.host + self.author_page_url();
    });
}

user = ko.observable(null);

init_user = function(data) {
    user(new UserVM(data));
}


AuthorVM = function(data) {
    var self = this;
    self.is_online = ko.observable(false);
    self.is_followed = ko.observable(false);

    UserVM.call(self, data);

    // is this author the logged in user?
    self.same_as_logged_in = ko.computed(function() {
        return user() && user().id() == self.id();
    });

    // logged in user chats with this author
    self.chat = function() {
        // open chat and focus it
        open_chat_with(self.id(), true);
    }

    self.follow_sending = ko.observable(false);
    // logged in user starts following this author
    self.follow = function() {
        if(self.follow_sending()) { return; }
        var url = "/follow/" + self.id();
        requests.post(url).
            success(function(response) {
                self.is_followed(true);
                var author_data = response;
                // add author to followings list
                buddy_list.add_buddy_data(author_data);
                // broadcast event to other sockets
                sockets.self_broadcast_others('started_following', author_data);
            }).
            error(function(response) {
                alert(response.error);
            }).
            complete(function() {
                self.follow_sending(false);
            }).
            send();
        self.follow_sending(true);
    }

    self.unfollow = function() {
        if(self.follow_sending()) { return; }
        var url = "/unfollow/" + self.id();
        requests.post(url).
            success(function(response) {
                var author_id = self.id()
                self.is_followed(false);
                // remove from buddy list
                buddy_list.remove_buddy_by_id(author_id);
                // broadcast event to other sockets
                sockets.self_broadcast_others('stopped_following', author_id);
            }).
            error(function(response) {
                alert(response.error);
            }).
            complete(function() {
                self.follow_sending(false);
            }).
            send();
        self.follow_sending(true);
    }

    // listen for the user coming online
    if(!self.same_as_logged_in()) {
        var channel = "online:" + self.id();
        sockets.listen(channel, function(data) {
            self.is_online(data);
        });
    }

    self.go_to_page = function() {
        location.href = author_page_url(self);
    }
}

Account = function(data) {
    var self = this;

    UserVM.call(self, data);

    self.switch_to = function() {
        requests.post("/switch_account").data({account_id: self.id()}).
            success(function() {
                reload_root();
            }).error(function(response) {
                alert("Failed: " + response.error);
            }).send();
    }
}

// XXX instead of an AccountsMenu class, maybe we should have an Infobar class,
// and that class should know what popup we're showing?

AccountsMenu = function() {
    var self = this;
    self.accounts = ko.observable([]);

    self.init_accounts = function(accounts_data) {
        var accounts = []
        for(var i = 0; i < accounts_data.length; i++) {
            accounts.push(new Account(accounts_data[i]));
        }
        self.accounts(accounts);
    }
}

accounts_menu = new AccountsMenu();

window.LoginForm = function() {
    var self;
    self = this;
    self.username = ko.observable();
    self.password = ko.observable();
    self.loading = ko.observable(false);
    self.submit = function(form) {
        // workd-around for autofilling login forms by browsers not triggering value updates
        $(form).find(':input').trigger('input');
        var data = { username: self.username(), password: self.password() };
        requests.post("/login").data(data).
            success(function(response) {
                return window.location.href = "/pad";
                // no need to clear username and password since we're redirecting
            }).
            error(function(response) {
                alert(response.error);
            }).
            complete(function() {
                self.loading(false);
            }).
            send();
        self.loading(true);
    };
    return this;
};

login = new LoginForm();

infobarPopup = ko.observable('');

hideInfobarPopup = function() {
    infobarPopup('');
};

showInfobarPopup = function(name) {
    var showit = function() {
        infobarPopup('template_' + name + '_popup');
    }
    // this is necessary to work around the auto popup removal mechanism ..
    setTimeout(showit, 0);
}

show_signup_form = function() {
    showInfobarPopup('signup');
}
show_login_form = function() {
    showInfobarPopup('login');
}
show_accounts_menu = function() {
    showInfobarPopup('accounts_menu');
}

login_as_demo = function() {
    requests.post("/demo").success(function() {
        console.log("success!");
        location.href = "/";
    }).send();
}

window.SignupForm = function() {
    var self;
    self = this;
    self.name = ko.observable();
    self.email = ko.observable();
    self.username = ko.observable();
    self.password = ko.observable();
    self.loading = ko.observable(false);
    self.error = auto_cleared_observable('');
    self.submit = function(form) {
        var name, passwd;
        var data = { name: self.name(), email: self.email(), username: self.username(), password: self.password() };
        requests.post("/register").data(data).
            success(function(response) {
                location.href = "/pad";
                // no need to clear username and password since we're redirecting
            }).
            error(function(response) {
                self.error(response.error);
            }).
            complete(function() {
                self.loading(false);
            }).
            send();
        self.loading(true);
    };
    return this;
};

signup = new SignupForm();

// XXX really? ko init happens here?
$(function() {
    ko.applyBindings();
});
