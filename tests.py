import os
import unittest

# MOST IMPORTANT THING!
# **MUST** COME BEFORE IMPORTING APP
os.environ['FLASK_TESTING'] = '1'

# monkey patch here to prevent annoying "keyerror in threading"
from gevent import monkey
monkey.patch_all()

from app import app, db

import json

from flask import request


# Some helpers
class JsonHelper(object):

    @classmethod
    def parse_json(cls, rv):
        """Adds a `json` field to the response by parsing the `data` field"""
        rv.json = json.loads(rv.data)

    # to be used in **notation with post/put requests
    # e.g. self.app.post("/url", **self.make_json(a=10, b="what"))
    @classmethod
    def make_json(cls, **kwargs):
        data = json.dumps(kwargs)
        content_type = "application/json"
        return dict(data=data, content_type=content_type)

    @classmethod
    def post(cls, client, url, **kwargs):
        rv = client.post(url, **cls.make_json(**kwargs))
        cls.parse_json(rv) # adds a .json to rv
        return rv

    @classmethod
    def put(cls, client, url, **kwargs):
        rv = client.put(url, **cls.make_json(**kwargs))
        cls.parse_json(rv) # adds a .json to rv
        return rv

    @classmethod
    def get(cls, client, url):
        rv = client.get(url)
        cls.parse_json(rv) # adds a .json to rv
        return rv

class AppTestCase(unittest.TestCase):

    def setUp(self):
        self.app = app.test_client()

    def tearDown(self):
        pass

    # json post request with self.app
    def post_json(self, *args, **kwargs):
        return JsonHelper.post(self.app, *args, **kwargs)

    # json put request with self.app
    def put_json(self, *args, **kwargs):
        return JsonHelper.put(self.app, *args, **kwargs)

    # json get request with self.app
    def get_json(self, *args, **kwargs):
        return JsonHelper.get(self.app, *args, **kwargs)

# ------ tests start here ------

import auth
import thoughts
import comments

class ThoughtsTestCase(unittest.TestCase):

    def setUp(self):
        app.test_request_context().push()
        auth.login_as_demo()
        request.user = auth.get_user()
        self.app = app.test_client()

    def test_thoughts(self):
        count = thoughts.Thought.select().count()
        th = thoughts.create_thought("Hello world")
        th = json.loads(th.data)
        count += 1
        assert thoughts.Thought.select().count() == count
        thoughts.edit_thought(th['id'], "What's up world")
        assert thoughts.Thought.select().count() == count

        # make assertions about the content object
        th = thoughts.Thought.get(thoughts.Thought.id == th['id'])
        # th is now a proper db object
        assert th.content.text == "What's up world"
        assert th.content.parent.text == "Hello world"

    def test_comments(self):
        th = comments.Thread.create(op=request.user)
        text1 = "Neque augue magna aenean lacus"
        c = comments.create_comment(th.id, text1)
        c = json.loads(c.data)
        assert comments.Comment.get(comments.Comment.id == c['id']).content.text == text1
        # print "Created a comment"
        text2 = "Etiam curae magnis et accumsan nonumm"
        c2 = comments.edit_comment(c['id'], text2)
        c2 = json.loads(c2.data)
        assert comments.Comment.get(comments.Comment.id == c2['id']).content.text == text2
        # print "Edited a comment"


def same_objects(listy1, listy2):
    listy1 = [id_of_object(o) for o in listy1]
    listy2 = [id_of_object(o) for o in listy2]
    return set(listy1) == set(listy2)

import messages
from api import *

class ConversationsTestCase(unittest.TestCase):

    def test_conversation_finding(self):
        u1, u2, u3, u4 = [auth.User.create(username=x, password='x') for x in "asdf"]

        c1 = messages.Conversation.new(u1, u2, u3)
        c2 = messages.Conversation.new(u2, u3, u4)
        c3 = messages.Conversation.new(u1, u2)
        c4 = messages.Conversation.new(u2, u3)

        assert messages.Conversation.between(u1, u2, u3) == c1
        assert messages.Conversation.between(u2, u3, u1) == c1
        assert messages.Conversation.between(u1, u3, u2) == c1
        assert messages.Conversation.between(u2, u3, u4) == c2
        assert messages.Conversation.between(u4, u3, u2) == c2
        # assert messages.Conversation.between(u4, u3) == None
        assert messages.Conversation.between(u1, u2) == c3
        assert messages.Conversation.between(u2, u1) == c3
        assert messages.Conversation.between(u2, u3) == c4
        assert messages.Conversation.between(u3, u2) == c4

        # create some message objects
        # send a message from u1 to c1 (u2, u3)
        m1 = messages.send_message(u1, c1, "hello")
        # check the recipients() method works
        assert same_objects(m1.recipients(), [u2, u3])

import follow

class FollowingsTestCase(unittest.TestCase):

    def test_followers_queries(self):
        u1, u2, u3, u4, u5, u6, u7 = [auth.User.create(username=x, password='x') for x in "asdfghj"]

        u1_targets = [u2, u3, u6]
        for t in u1_targets: follow.follow(u1, t)

        q1 = follow.Following.by(u1)
        assert same_objects(q1, u1_targets)

        q2 = follow.Following.of(u2)
        assert same_objects(q2, [u1])

        # note: u4 is not followed by u1
        u4_followers = [u3, u6, u2, u5]
        for f in u4_followers: follow.follow(f, u4)

        q3 = follow.Following.of(u4)
        assert same_objects(q3, u4_followers)

        assert follow.Following.exists(u1, u2)
        follow.follow(u2, u1)
        assert follow.Following.exists(u2, u1)
        assert follow.Following.is_mutual(u1, u2)
        assert follow.Following.is_mutual(u2, u1)

    def test_muting_queries(self):
        u1, u2, u3, u4, u5, u6, u7 = [auth.User.create(username=x, password='x') for x in "asdfghj"]

        c = messages.Conversation.between(u1, u2, u3, u4)
        follow.mute(u1, c)

        assert same_objects(follow.muter_ids(c), [u1.id])


    def x_test_robustness(self):
        u1, u2, u3, u4, u5, u6, u7 = [auth.User.create(username=x, password='x') for x in "asdfghj"]
        b1 = follow.block(u1, u3)
        # duplicate should be ok ..
        b2 = follow.block(u1, u3)
        assert b1.id == b2.id

        f1 = follow.follow(u1, u2)
        f2 = follow.follow(u1, u2)
        assert f1.id == f2.id



if __name__ == '__main__':
    unittest.main()
