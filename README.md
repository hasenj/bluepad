BluePad
======

Microblogging, user-to-user chat, forums

Bluepad is Not a Social Network

At this stage it's mostly alpha/prototype

Not being actively developed

Setup
----------------------------

You need to create a virtualenv, and you should call it `venv`, (that's the dir name that's ignored by git).

Activate the virtual environment and install the required packages.

    virtualenv --no-site-packages venv
    source venv/bin/activate
    pip install -r requirements.txt

To run the local server:

    ./local.sh
